package lang;

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import beaver.Parser.Exception;
import java.io.PrintStream;
import java.io.File;
import java.lang.ProcessBuilder;
import lang.ast.Program;
import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.ast.ErrorMessage;
import java.io.FileOutputStream;


/**
 * Dumps the parsed Abstract Syntax Tree of a Calc program.
 */
public class Compiler {
	/**
	 * Entry point
	 * 
	 * @param args
	 */

	public static Object DrAST_root_node; // Enable debugging with DrAST

	public static String changeExtension(String filename, String newExtension) {
    	int index = filename.lastIndexOf('.');
    	if (index != -1) {
    	  return filename.substring(0, index) + newExtension;
    	} else {
    	  return filename + newExtension;
    	}
 	}
	public static void main(String[] args) {
		

		try {
			if (args.length != 1) {
				System.err.println("You must specify a source file on the command line!");
				printUsage();
				System.exit(1);
				return;
			}

			String filename = args[0];
			PreProc preProcessor = new PreProc();
						//preProcessor.fixSynt(filename);

			LangScanner scanner = new LangScanner(new FileReader(preProcessor.fixSynt(filename)));
			LangParser parser = new LangParser();
			Program program = (Program) parser.parse(scanner);
			//System.out.println(program.dumpTree());
			DrAST_root_node = program; // Enable debugging with DrAST

		if (!program.errors().isEmpty()) {
        System.err.println();
        System.err.println("Errors: ");
        for (ErrorMessage e: program.errors()) {
          System.err.println("- " + e);
          System.exit(1);
        }
      }else {
       // program.genCode(System.out,null);
	       
		    File assemblyFile = new File(changeExtension(filename, ".s"));
    		PrintStream out = new PrintStream(new FileOutputStream(assemblyFile));
    		program.genCode(out,null);
    		out.close();
			

		
    		// Generate object file.
    		
      }
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private static void printUsage() {
		System.err.println("Usage: DumpTree FILE");
		System.err.println("  where FILE is the file to be parsed");
	}
}
