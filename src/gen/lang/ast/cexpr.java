/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/lang.ast:40
 * @astdecl cexpr : Expr;
 * @production cexpr : {@link Expr};

 */
public abstract class cexpr extends Expr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/CodeGen.jrag:558
   */
  public void genEval(PrintStream out, IdDecl id) {
		throw new UnsupportedOperationException();
	}
  /**
   * @aspect CodeGen
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/CodeGen.jrag:764
   */
  public void genConditionalJump(PrintStream out,IdDecl id,String falseLabel) {
		throw new UnsupportedOperationException();
	}
  /**
   * @declaredat ASTNode:1
   */
  public cexpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    paramse_reset();
    nbrP_reset();
    getTypes_reset();
    decle_reset();
    getVarDef_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public cexpr clone() throws CloneNotSupportedException {
    cexpr node = (cexpr) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:42
   */
  @Deprecated
  public abstract cexpr fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:50
   */
  public abstract cexpr treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:58
   */
  public abstract cexpr treeCopy();
/** @apilevel internal */
protected boolean paramse_visited = false;
  /** @apilevel internal */
  private void paramse_reset() {
    paramse_computed = false;
    
    paramse_value = null;
    paramse_visited = false;
  }
  /** @apilevel internal */
  protected boolean paramse_computed = false;

  /** @apilevel internal */
  protected List<Expr> paramse_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/checkFuncCall.jrag:41
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/checkFuncCall.jrag:41")
  public List<Expr> paramse() {
    ASTState state = state();
    if (paramse_computed) {
      return paramse_value;
    }
    if (paramse_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.paramse().");
    }
    paramse_visited = true;
    state().enterLazyAttribute();
    paramse_value = new List<Expr>();
    paramse_computed = true;
    state().leaveLazyAttribute();
    paramse_visited = false;
    return paramse_value;
  }
/** @apilevel internal */
protected boolean nbrP_visited = false;
  /** @apilevel internal */
  private void nbrP_reset() {
    nbrP_computed = false;
    nbrP_visited = false;
  }
  /** @apilevel internal */
  protected boolean nbrP_computed = false;

  /** @apilevel internal */
  protected int nbrP_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/checkFuncCall.jrag:56
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/checkFuncCall.jrag:56")
  public int nbrP() {
    ASTState state = state();
    if (nbrP_computed) {
      return nbrP_value;
    }
    if (nbrP_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.nbrP().");
    }
    nbrP_visited = true;
    state().enterLazyAttribute();
    nbrP_value = 0;
    nbrP_computed = true;
    state().leaveLazyAttribute();
    nbrP_visited = false;
    return nbrP_value;
  }
/** @apilevel internal */
protected boolean getTypes_visited = false;
  /** @apilevel internal */
  private void getTypes_reset() {
    getTypes_computed = false;
    
    getTypes_value = null;
    getTypes_visited = false;
  }
  /** @apilevel internal */
  protected boolean getTypes_computed = false;

  /** @apilevel internal */
  protected HashMap<Integer, String> getTypes_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:40")
  public HashMap<Integer, String> getTypes() {
    ASTState state = state();
    if (getTypes_computed) {
      return getTypes_value;
    }
    if (getTypes_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.getTypes().");
    }
    getTypes_visited = true;
    state().enterLazyAttribute();
    getTypes_value = new  HashMap<Integer, String>();
    getTypes_computed = true;
    state().leaveLazyAttribute();
    getTypes_visited = false;
    return getTypes_value;
  }
  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:86
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:86")
  public IdDecl decle() {
    ASTState state = state();
    if (decle_computed) {
      return decle_value;
    }
    if (decle_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.decle().");
    }
    decle_visited = true;
    state().enterLazyAttribute();
    decle_value = getParent().Define_decle(this, null);
    decle_computed = true;
    state().leaveLazyAttribute();
    decle_visited = false;
    return decle_value;
  }
/** @apilevel internal */
protected boolean decle_visited = false;
  /** @apilevel internal */
  private void decle_reset() {
    decle_computed = false;
    
    decle_value = null;
    decle_visited = false;
  }
  /** @apilevel internal */
  protected boolean decle_computed = false;

  /** @apilevel internal */
  protected IdDecl decle_value;

  /**
   * @attribute inh
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:3")
  public var_def getVarDef() {
    ASTState state = state();
    if (getVarDef_computed) {
      return getVarDef_value;
    }
    if (getVarDef_visited) {
      throw new RuntimeException("Circular definition of attribute cexpr.getVarDef().");
    }
    getVarDef_visited = true;
    state().enterLazyAttribute();
    getVarDef_value = getParent().Define_getVarDef(this, null);
    getVarDef_computed = true;
    state().leaveLazyAttribute();
    getVarDef_visited = false;
    return getVarDef_value;
  }
/** @apilevel internal */
protected boolean getVarDef_visited = false;
  /** @apilevel internal */
  private void getVarDef_reset() {
    getVarDef_computed = false;
    
    getVarDef_value = null;
    getVarDef_visited = false;
  }
  /** @apilevel internal */
  protected boolean getVarDef_computed = false;

  /** @apilevel internal */
  protected var_def getVarDef_value;


}
