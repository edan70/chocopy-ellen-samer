/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/lang.ast:14
 * @astdecl Type : ASTNode;
 * @production Type : {@link ASTNode};

 */
public abstract class Type extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public Type() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    compatibleType_Type_reset();
    isListType_reset();
    idif_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:24
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  public Type clone() throws CloneNotSupportedException {
    Type node = (Type) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:40
   */
  @Deprecated
  public abstract Type fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:48
   */
  public abstract Type treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:56
   */
  public abstract Type treeCopy();
/** @apilevel internal */
protected java.util.Set compatibleType_Type_visited;
  /** @apilevel internal */
  private void compatibleType_Type_reset() {
    compatibleType_Type_values = null;
    compatibleType_Type_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map compatibleType_Type_values;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:185
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:185")
  public boolean compatibleType(Type type) {
    Object _parameters = type;
    if (compatibleType_Type_visited == null) compatibleType_Type_visited = new java.util.HashSet(4);
    if (compatibleType_Type_values == null) compatibleType_Type_values = new java.util.HashMap(4);
    ASTState state = state();
    if (compatibleType_Type_values.containsKey(_parameters)) {
      return (Boolean) compatibleType_Type_values.get(_parameters);
    }
    if (compatibleType_Type_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Type.compatibleType(Type).");
    }
    compatibleType_Type_visited.add(_parameters);
    state().enterLazyAttribute();
    boolean compatibleType_Type_value = this.toString().equals(type.toString());
    compatibleType_Type_values.put(_parameters, compatibleType_Type_value);
    state().leaveLazyAttribute();
    compatibleType_Type_visited.remove(_parameters);
    return compatibleType_Type_value;
  }
/** @apilevel internal */
protected boolean isListType_visited = false;
  /** @apilevel internal */
  private void isListType_reset() {
    isListType_computed = false;
    isListType_visited = false;
  }
  /** @apilevel internal */
  protected boolean isListType_computed = false;

  /** @apilevel internal */
  protected boolean isListType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:188
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:188")
  public boolean isListType() {
    ASTState state = state();
    if (isListType_computed) {
      return isListType_value;
    }
    if (isListType_visited) {
      throw new RuntimeException("Circular definition of attribute Type.isListType().");
    }
    isListType_visited = true;
    state().enterLazyAttribute();
    isListType_value = false;
    isListType_computed = true;
    state().leaveLazyAttribute();
    isListType_visited = false;
    return isListType_value;
  }
/** @apilevel internal */
protected boolean idif_visited = false;
  /** @apilevel internal */
  private void idif_reset() {
    idif_computed = false;
    
    idif_value = null;
    idif_visited = false;
  }
  /** @apilevel internal */
  protected boolean idif_computed = false;

  /** @apilevel internal */
  protected String idif_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:166
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:166")
  public String idif() {
    ASTState state = state();
    if (idif_computed) {
      return idif_value;
    }
    if (idif_visited) {
      throw new RuntimeException("Circular definition of attribute Type.idif().");
    }
    idif_visited = true;
    state().enterLazyAttribute();
    idif_value = "-";
    idif_computed = true;
    state().leaveLazyAttribute();
    idif_visited = false;
    return idif_value;
  }

}
