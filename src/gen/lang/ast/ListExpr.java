/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/lang.ast:47
 * @astdecl ListExpr : cexpr ::= Expr*;
 * @production ListExpr : {@link cexpr} ::= <span class="component">{@link Expr}*</span>;

 */
public class ListExpr extends cexpr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/CodeGen.jrag:407
   */
  public void genEval(PrintStream out, IdDecl id){
		for (int i =  getNumExpr()-1; i>=0; i--){
			getExpr(i).genEval(out,id);
			out.println("        push %rax             # pushing an element of the list to the stack");
		}
		int size = 8;
	
		out.println("        movq $"+ getNumExpr()+", %rax   # size of the list = "+ getNumExpr()*size  );
		out.println("        push %rax");
		out.println("        pop %rsi                   	# moving list's size to rsi ");
		out.println("        movq  %rsi, %rcx        			# moving list's size to rcx ");
		out.println("        push %rcx              			# pushing real list size ");
		out.println("        addq $1, %rsi           			# increasing the size to save the size");
		out.println("        imulq  $8, %rsi        			# mul list size with 8  ");
		out.println("        pop %rcx              				# moving list's size to rcx ");
		out.println("		 		 movq heap_pointer(%rip) , %rax   # moving the heap pointer to rax");
		out.println("        movq %rcx, (%rax)         		# moving list's element to list ");
		for ( int i = 1; i <= getNumExpr();i++){
			out.println("        pop %rdi              					# moving list elem to rdi ");
			int offset = 8*i;
			out.println("        movq %rdi,"+offset+"(%rax)   	# moving list's element to list ");
		}
		out.println("        addq %rsi, heap_pointer(%rip)  	# increasing the size to save the size");
	}
  /**
   * @declaredat ASTNode:1
   */
  public ListExpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Expr"},
    type = {"List<Expr>"},
    kind = {"List"}
  )
  public ListExpr(List<Expr> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
    correctTypeExprTypes_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public ListExpr clone() throws CloneNotSupportedException {
    ListExpr node = (ListExpr) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public ListExpr copy() {
    try {
      ListExpr node = (ListExpr) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public ListExpr fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public ListExpr treeCopyNoTransform() {
    ListExpr tree = (ListExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public ListExpr treeCopy() {
    ListExpr tree = (ListExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the Expr list.
   * @param list The new list node to be used as the Expr list.
   * @apilevel high-level
   */
  public ListExpr setExprList(List<Expr> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the Expr list.
   * @return Number of children in the Expr list.
   * @apilevel high-level
   */
  public int getNumExpr() {
    return getExprList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Expr list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Expr list.
   * @apilevel low-level
   */
  public int getNumExprNoTransform() {
    return getExprListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Expr list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Expr list.
   * @apilevel high-level
   */
  public Expr getExpr(int i) {
    return (Expr) getExprList().getChild(i);
  }
  /**
   * Check whether the Expr list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasExpr() {
    return getExprList().getNumChild() != 0;
  }
  /**
   * Append an element to the Expr list.
   * @param node The element to append to the Expr list.
   * @apilevel high-level
   */
  public ListExpr addExpr(Expr node) {
    List<Expr> list = (parent == null) ? getExprListNoTransform() : getExprList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public ListExpr addExprNoTransform(Expr node) {
    List<Expr> list = getExprListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Expr list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public ListExpr setExpr(Expr node, int i) {
    List<Expr> list = getExprList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Expr list.
   * @return The node representing the Expr list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Expr")
  public List<Expr> getExprList() {
    List<Expr> list = (List<Expr>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Expr list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Expr list.
   * @apilevel low-level
   */
  public List<Expr> getExprListNoTransform() {
    return (List<Expr>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Expr list without
   * triggering rewrites.
   */
  public Expr getExprNoTransform(int i) {
    return (Expr) getExprListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Expr list.
   * @return The node representing the Expr list.
   * @apilevel high-level
   */
  public List<Expr> getExprs() {
    return getExprList();
  }
  /**
   * Retrieves the Expr list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Expr list.
   * @apilevel low-level
   */
  public List<Expr> getExprsNoTransform() {
    return getExprListNoTransform();
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Type types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:83
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:83")
  public Type types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute ListExpr.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = types_compute();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /** @apilevel internal */
  private Type types_compute() {
  		Type startType = getExpr(0).types();
  		for (Expr elem : getExprs()){
  			if (!elem.types().compatibleType(startType)){
  				return new ListType(unknownType());
  			}
  			
  		}
  		return new ListType(startType);
  	}
/** @apilevel internal */
protected boolean correctTypeExprTypes_visited = false;
  /** @apilevel internal */
  private void correctTypeExprTypes_reset() {
    correctTypeExprTypes_computed = false;
    correctTypeExprTypes_visited = false;
  }
  /** @apilevel internal */
  protected boolean correctTypeExprTypes_computed = false;

  /** @apilevel internal */
  protected boolean correctTypeExprTypes_value;

  /**
   * @attribute syn
   * @aspect checkFuncCall
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/checkFuncCall.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="checkFuncCall", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/checkFuncCall.jrag:64")
  public boolean correctTypeExprTypes() {
    ASTState state = state();
    if (correctTypeExprTypes_computed) {
      return correctTypeExprTypes_value;
    }
    if (correctTypeExprTypes_visited) {
      throw new RuntimeException("Circular definition of attribute ListExpr.correctTypeExprTypes().");
    }
    correctTypeExprTypes_visited = true;
    state().enterLazyAttribute();
    correctTypeExprTypes_value = correctTypeExprTypes_compute();
    correctTypeExprTypes_computed = true;
    state().leaveLazyAttribute();
    correctTypeExprTypes_visited = false;
    return correctTypeExprTypes_value;
  }
  /** @apilevel internal */
  private boolean correctTypeExprTypes_compute() {
  		Type startType = getExpr(0).types();
  		for (Expr elem : getExprs()){
  			if (!elem.types().compatibleType(startType)){
  				return false;
  			}
  		}
  		return true;
  
  	}
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:80
    if (!correctTypeExprTypes()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!correctTypeExprTypes()) {
      collection.add(error("List has incompatible types"));
    }
  }

}
