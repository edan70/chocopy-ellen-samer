/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @astdecl ASTNode;
 * @production ASTNode;

 */
public class ASTNode<T extends ASTNode> extends beaver.Symbol implements Cloneable {
  /**
   * @aspect Errors
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:22
   */
  protected ErrorMessage error(String message) {
		return new ErrorMessage(message, getLine(getStart()));
	}
  /**
   * @aspect DumpTree
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/dumpTree.jrag:9
   */
  private static final String DUMP_TREE_INDENT = "  ";
  /**
   * @aspect DumpTree
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/dumpTree.jrag:11
   */
  public String dumpTree() {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		dumpTree(new PrintStream(bytes));
		return bytes.toString();
	}
  /**
   * @aspect DumpTree
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/dumpTree.jrag:17
   */
  public void dumpTree(PrintStream out) {
		dumpTree(out, "");
		out.flush();
	}
  /**
   * @aspect DumpTree
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/dumpTree.jrag:22
   */
  public void dumpTree(PrintStream out, String indent) {
		out.print(indent + getClass().getSimpleName());
		out.println(getTokens());
		String childIndent = indent + DUMP_TREE_INDENT;
		for (ASTNode child : astChildren()) {
			if (child == null) {
				out.println(childIndent + "null");
			} else {
				child.dumpTree(out, childIndent);
			}
		}
	}
  /**
   * @aspect DumpTree
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/dumpTree.jrag:35
   */
  public String getTokens() {
		java.util.TreeSet<java.lang.reflect.Method> methods = new java.util.TreeSet<>(
				new java.util.Comparator<java.lang.reflect.Method>() {
					public int compare(java.lang.reflect.Method m1, java.lang.reflect.Method m2) {
						return m1.getName().compareTo(m2.getName());
					}
				});

		methods.addAll(java.util.Arrays.asList(getClass().getMethods()));

		String result = "";
		for (java.lang.reflect.Method method : methods) {
			ASTNodeAnnotation.Token token = method.getAnnotation(ASTNodeAnnotation.Token.class);
			if (token != null) {
				try {
					result += String.format(" %s=\"%s\"", token.name(), method.invoke(this));
				} catch (IllegalAccessException ignored) {
				} catch (InvocationTargetException ignored) {
				}
			}
		}
		return result;
	}
  /**
   * @declaredat ASTNode:1
   */
  public ASTNode() {
    super();
    init$Children();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:11
   */
  public void init$Children() {
  }
  /**
   * Cached child index. Child indices are assumed to never change (AST should
   * not change after construction).
   * @apilevel internal
   * @declaredat ASTNode:18
   */
  private int childIndex = -1;
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  public int getIndexOfChild(ASTNode node) {
    if (node == null) {
      return -1;
    }
    if (node.childIndex >= 0) {
      return node.childIndex;
    }
    for (int i = 0; children != null && i < children.length; i++) {
      if (children[i] == node) {
        node.childIndex = i;
        return i;
      }
    }
    return -1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public static final boolean generatedWithCacheCycle = true;
  /** @apilevel low-level 
   * @declaredat ASTNode:41
   */
  protected ASTNode parent;
  /** @apilevel low-level 
   * @declaredat ASTNode:44
   */
  protected ASTNode[] children;
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  private static ASTState state = new ASTState();
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  public final ASTState state() {
    return state;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:56
   */
  public final static ASTState resetState() {
    return state = new ASTState();
  }
  /**
   * @return an iterator that can be used to iterate over the children of this node.
   * The iterator does not allow removing children.
   * @declaredat ASTNode:65
   */
  public java.util.Iterator<T> astChildIterator() {
    return new java.util.Iterator<T>() {
      private int index = 0;

      @Override
      public boolean hasNext() {
        return index < getNumChild();
      }

      @Override
      public T next() {
        return hasNext() ? (T) getChild(index++) : null;
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  /** @return an object that can be used to iterate over the children of this node 
   * @declaredat ASTNode:87
   */
  public Iterable<T> astChildren() {
    return new Iterable<T>() {
      @Override
      public java.util.Iterator<T> iterator() {
        return astChildIterator();
      }
    };
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:97
   */
  public T getChild(int i) {
    ASTNode child = getChildNoTransform(i);
    return (T) child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:102
   */
  public ASTNode addChild(T node) {
    setChild(node, getNumChildNoTransform());
    return this;
  }
  /**
   * Gets a child without triggering rewrites.
   * @apilevel low-level
   * @declaredat ASTNode:110
   */
  public T getChildNoTransform(int i) {
    if (children == null) {
      return null;
    }
    T child = (T) children[i];
    return child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:118
   */
  protected int numChildren;
  /** @apilevel low-level 
   * @declaredat ASTNode:121
   */
  protected int numChildren() {
    return numChildren;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:126
   */
  public int getNumChild() {
    return numChildren();
  }
  /**
   * Behaves like getNumChild, but does not invoke AST transformations (rewrites).
   * @apilevel low-level
   * @declaredat ASTNode:134
   */
  public final int getNumChildNoTransform() {
    return numChildren();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:138
   */
  public ASTNode setChild(ASTNode node, int i) {
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
    } else if (i >= children.length) {
      ASTNode c[] = new ASTNode[i << 1];
      System.arraycopy(children, 0, c, 0, children.length);
      children = c;
    }
    children[i] = node;
    if (i >= numChildren) {
      numChildren = i+1;
    }
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
    return this;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:157
   */
  public ASTNode insertChild(ASTNode node, int i) {
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
      children[i] = node;
    } else {
      ASTNode c[] = new ASTNode[children.length + 1];
      System.arraycopy(children, 0, c, 0, i);
      c[i] = node;
      if (i < children.length) {
        System.arraycopy(children, i, c, i+1, children.length-i);
        for(int j = i+1; j < c.length; ++j) {
          if (c[j] != null) {
            c[j].childIndex = j;
          }
        }
      }
      children = c;
    }
    numChildren++;
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
    return this;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:183
   */
  public void removeChild(int i) {
    if (children != null) {
      ASTNode child = (ASTNode) children[i];
      if (child != null) {
        child.parent = null;
        child.childIndex = -1;
      }
      // Adding a check of this instance to make sure its a List, a move of children doesn't make
      // any sense for a node unless its a list. Also, there is a problem if a child of a non-List node is removed
      // and siblings are moved one step to the right, with null at the end.
      if (this instanceof List || this instanceof Opt) {
        System.arraycopy(children, i+1, children, i, children.length-i-1);
        children[children.length-1] = null;
        numChildren--;
        // fix child indices
        for(int j = i; j < numChildren; ++j) {
          if (children[j] != null) {
            child = (ASTNode) children[j];
            child.childIndex = j;
          }
        }
      } else {
        children[i] = null;
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:210
   */
  public ASTNode getParent() {
    return (ASTNode) parent;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:214
   */
  public void setParent(ASTNode node) {
    parent = node;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:279
   */
  public void flushTreeCache() {
    flushCache();
    if (children != null) {
      for (int i = 0; i < children.length; i++) {
        if (children[i] != null) {
          ((ASTNode) children[i]).flushTreeCache();
        }
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:290
   */
  public void flushCache() {
    flushAttrAndCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:294
   */
  public void flushAttrAndCollectionCache() {
    flushAttrCache();
    flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:299
   */
  public void flushAttrCache() {
    localIndex_reset();
    isExpr_reset();
    lastNode_reset();
    prevNode_int_reset();
    program_reset();
    unknownDecl_reset();
    prevNode_reset();
    boolType_reset();
    intType_reset();
    anyType_reset();
    listType_reset();
    stringType_reset();
    objType_reset();
    unknownType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:316
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:319
   */
  public ASTNode<T> clone() throws CloneNotSupportedException {
    ASTNode node = (ASTNode) super.clone();
    node.flushAttrAndCollectionCache();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:325
   */
  public ASTNode<T> copy() {
    try {
      ASTNode node = (ASTNode) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:344
   */
  @Deprecated
  public ASTNode<T> fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:354
   */
  public ASTNode<T> treeCopyNoTransform() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:374
   */
  public ASTNode<T> treeCopy() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Performs a full traversal of the tree using getChild to trigger rewrites
   * @apilevel low-level
   * @declaredat ASTNode:391
   */
  public void doFullTraversal() {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).doFullTraversal();
    }
  }
  /**
   * @aspect <NoAspect>
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:26
   */
    /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).collect_contributors_Program_errors(_root, _map);
    }
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
  }

/** @apilevel internal */
protected boolean localIndex_visited = false;
  /** @apilevel internal */
  private void localIndex_reset() {
    localIndex_computed = false;
    localIndex_visited = false;
  }
  /** @apilevel internal */
  protected boolean localIndex_computed = false;

  /** @apilevel internal */
  protected int localIndex_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:28
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:28")
  public int localIndex() {
    ASTState state = state();
    if (localIndex_computed) {
      return localIndex_value;
    }
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.localIndex().");
    }
    localIndex_visited = true;
    state().enterLazyAttribute();
    localIndex_value = prevNode().localIndex();
    localIndex_computed = true;
    state().leaveLazyAttribute();
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
protected boolean isExpr_visited = false;
  /** @apilevel internal */
  private void isExpr_reset() {
    isExpr_computed = false;
    isExpr_visited = false;
  }
  /** @apilevel internal */
  protected boolean isExpr_computed = false;

  /** @apilevel internal */
  protected boolean isExpr_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:203
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:203")
  public boolean isExpr() {
    ASTState state = state();
    if (isExpr_computed) {
      return isExpr_value;
    }
    if (isExpr_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.isExpr().");
    }
    isExpr_visited = true;
    state().enterLazyAttribute();
    isExpr_value = false;
    isExpr_computed = true;
    state().leaveLazyAttribute();
    isExpr_visited = false;
    return isExpr_value;
  }
/** @apilevel internal */
protected boolean lastNode_visited = false;
  /** @apilevel internal */
  private void lastNode_reset() {
    lastNode_computed = false;
    
    lastNode_value = null;
    lastNode_visited = false;
  }
  /** @apilevel internal */
  protected boolean lastNode_computed = false;

  /** @apilevel internal */
  protected ASTNode lastNode_value;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:208
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:208")
  public ASTNode lastNode() {
    ASTState state = state();
    if (lastNode_computed) {
      return lastNode_value;
    }
    if (lastNode_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.lastNode().");
    }
    lastNode_visited = true;
    state().enterLazyAttribute();
    lastNode_value = prevNode(getNumChild());
    lastNode_computed = true;
    state().leaveLazyAttribute();
    lastNode_visited = false;
    return lastNode_value;
  }
/** @apilevel internal */
protected java.util.Set prevNode_int_visited;
  /** @apilevel internal */
  private void prevNode_int_reset() {
    prevNode_int_values = null;
    prevNode_int_visited = null;
  }
  /** @apilevel internal */
  protected java.util.Map prevNode_int_values;

  /**
   * @attribute syn
   * @aspect paramAdrInd
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:209
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:209")
  public ASTNode prevNode(int i) {
    Object _parameters = i;
    if (prevNode_int_visited == null) prevNode_int_visited = new java.util.HashSet(4);
    if (prevNode_int_values == null) prevNode_int_values = new java.util.HashMap(4);
    ASTState state = state();
    if (prevNode_int_values.containsKey(_parameters)) {
      return (ASTNode) prevNode_int_values.get(_parameters);
    }
    if (prevNode_int_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute ASTNode.prevNode(int).");
    }
    prevNode_int_visited.add(_parameters);
    state().enterLazyAttribute();
    ASTNode prevNode_int_value = prevNode_compute(i);
    prevNode_int_values.put(_parameters, prevNode_int_value);
    state().leaveLazyAttribute();
    prevNode_int_visited.remove(_parameters);
    return prevNode_int_value;
  }
  /** @apilevel internal */
  private ASTNode prevNode_compute(int i) {
  		if(i>0){
  			if(getChild(i-1).isExpr()){
  							return getChild(i-2).lastNode();
  
  			}
  			return getChild(i-1).lastNode();
  		}
  		return this;
  	}
  /**
   * @attribute inh
   * @aspect Errors
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:28
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Errors", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:28")
  public Program program() {
    ASTState state = state();
    if (program_computed) {
      return program_value;
    }
    if (program_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.program().");
    }
    program_visited = true;
    state().enterLazyAttribute();
    program_value = getParent().Define_program(this, null);
    program_computed = true;
    state().leaveLazyAttribute();
    program_visited = false;
    return program_value;
  }
/** @apilevel internal */
protected boolean program_visited = false;
  /** @apilevel internal */
  private void program_reset() {
    program_computed = false;
    
    program_value = null;
    program_visited = false;
  }
  /** @apilevel internal */
  protected boolean program_computed = false;

  /** @apilevel internal */
  protected Program program_value;

  /**
   * @attribute inh
   * @aspect UnknownDecl
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/UnknownDecl.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UnknownDecl", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/UnknownDecl.jrag:3")
  public UnknownDecl unknownDecl() {
    ASTState state = state();
    if (unknownDecl_computed) {
      return unknownDecl_value;
    }
    if (unknownDecl_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.unknownDecl().");
    }
    unknownDecl_visited = true;
    state().enterLazyAttribute();
    unknownDecl_value = getParent().Define_unknownDecl(this, null);
    unknownDecl_computed = true;
    state().leaveLazyAttribute();
    unknownDecl_visited = false;
    return unknownDecl_value;
  }
/** @apilevel internal */
protected boolean unknownDecl_visited = false;
  /** @apilevel internal */
  private void unknownDecl_reset() {
    unknownDecl_computed = false;
    
    unknownDecl_value = null;
    unknownDecl_visited = false;
  }
  /** @apilevel internal */
  protected boolean unknownDecl_computed = false;

  /** @apilevel internal */
  protected UnknownDecl unknownDecl_value;

  /**
   * @attribute inh
   * @aspect paramAdrInd
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:206
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="paramAdrInd", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:206")
  public ASTNode prevNode() {
    ASTState state = state();
    if (prevNode_computed) {
      return prevNode_value;
    }
    if (prevNode_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.prevNode().");
    }
    prevNode_visited = true;
    state().enterLazyAttribute();
    prevNode_value = getParent().Define_prevNode(this, null);
    prevNode_computed = true;
    state().leaveLazyAttribute();
    prevNode_visited = false;
    return prevNode_value;
  }
/** @apilevel internal */
protected boolean prevNode_visited = false;
  /** @apilevel internal */
  private void prevNode_reset() {
    prevNode_computed = false;
    
    prevNode_value = null;
    prevNode_visited = false;
  }
  /** @apilevel internal */
  protected boolean prevNode_computed = false;

  /** @apilevel internal */
  protected ASTNode prevNode_value;

  /**
   * @attribute inh
   * @aspect BoolType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:3
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="BoolType", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:3")
  public BoolType boolType() {
    ASTState state = state();
    if (boolType_computed) {
      return boolType_value;
    }
    if (boolType_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.boolType().");
    }
    boolType_visited = true;
    state().enterLazyAttribute();
    boolType_value = getParent().Define_boolType(this, null);
    boolType_computed = true;
    state().leaveLazyAttribute();
    boolType_visited = false;
    return boolType_value;
  }
/** @apilevel internal */
protected boolean boolType_visited = false;
  /** @apilevel internal */
  private void boolType_reset() {
    boolType_computed = false;
    
    boolType_value = null;
    boolType_visited = false;
  }
  /** @apilevel internal */
  protected boolean boolType_computed = false;

  /** @apilevel internal */
  protected BoolType boolType_value;

  /**
   * @attribute inh
   * @aspect IntType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="IntType", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:12")
  public IntType intType() {
    ASTState state = state();
    if (intType_computed) {
      return intType_value;
    }
    if (intType_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.intType().");
    }
    intType_visited = true;
    state().enterLazyAttribute();
    intType_value = getParent().Define_intType(this, null);
    intType_computed = true;
    state().leaveLazyAttribute();
    intType_visited = false;
    return intType_value;
  }
/** @apilevel internal */
protected boolean intType_visited = false;
  /** @apilevel internal */
  private void intType_reset() {
    intType_computed = false;
    
    intType_value = null;
    intType_visited = false;
  }
  /** @apilevel internal */
  protected boolean intType_computed = false;

  /** @apilevel internal */
  protected IntType intType_value;

  /**
   * @attribute inh
   * @aspect AnyType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="AnyType", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:20")
  public AnyType anyType() {
    ASTState state = state();
    if (anyType_computed) {
      return anyType_value;
    }
    if (anyType_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.anyType().");
    }
    anyType_visited = true;
    state().enterLazyAttribute();
    anyType_value = getParent().Define_anyType(this, null);
    anyType_computed = true;
    state().leaveLazyAttribute();
    anyType_visited = false;
    return anyType_value;
  }
/** @apilevel internal */
protected boolean anyType_visited = false;
  /** @apilevel internal */
  private void anyType_reset() {
    anyType_computed = false;
    
    anyType_value = null;
    anyType_visited = false;
  }
  /** @apilevel internal */
  protected boolean anyType_computed = false;

  /** @apilevel internal */
  protected AnyType anyType_value;

  /**
   * @attribute inh
   * @aspect ListType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="ListType", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:30")
  public ListType listType() {
    ASTState state = state();
    if (listType_computed) {
      return listType_value;
    }
    if (listType_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.listType().");
    }
    listType_visited = true;
    state().enterLazyAttribute();
    listType_value = getParent().Define_listType(this, null);
    listType_computed = true;
    state().leaveLazyAttribute();
    listType_visited = false;
    return listType_value;
  }
/** @apilevel internal */
protected boolean listType_visited = false;
  /** @apilevel internal */
  private void listType_reset() {
    listType_computed = false;
    
    listType_value = null;
    listType_visited = false;
  }
  /** @apilevel internal */
  protected boolean listType_computed = false;

  /** @apilevel internal */
  protected ListType listType_value;

  /**
   * @attribute inh
   * @aspect StringType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:39
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="StringType", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:39")
  public StringType stringType() {
    ASTState state = state();
    if (stringType_computed) {
      return stringType_value;
    }
    if (stringType_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.stringType().");
    }
    stringType_visited = true;
    state().enterLazyAttribute();
    stringType_value = getParent().Define_stringType(this, null);
    stringType_computed = true;
    state().leaveLazyAttribute();
    stringType_visited = false;
    return stringType_value;
  }
/** @apilevel internal */
protected boolean stringType_visited = false;
  /** @apilevel internal */
  private void stringType_reset() {
    stringType_computed = false;
    
    stringType_value = null;
    stringType_visited = false;
  }
  /** @apilevel internal */
  protected boolean stringType_computed = false;

  /** @apilevel internal */
  protected StringType stringType_value;

  /**
   * @attribute inh
   * @aspect ObjType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:48
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="ObjType", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:48")
  public ObjType objType() {
    ASTState state = state();
    if (objType_computed) {
      return objType_value;
    }
    if (objType_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.objType().");
    }
    objType_visited = true;
    state().enterLazyAttribute();
    objType_value = getParent().Define_objType(this, null);
    objType_computed = true;
    state().leaveLazyAttribute();
    objType_visited = false;
    return objType_value;
  }
/** @apilevel internal */
protected boolean objType_visited = false;
  /** @apilevel internal */
  private void objType_reset() {
    objType_computed = false;
    
    objType_value = null;
    objType_visited = false;
  }
  /** @apilevel internal */
  protected boolean objType_computed = false;

  /** @apilevel internal */
  protected ObjType objType_value;

  /**
   * @attribute inh
   * @aspect UnknownType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:59
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UnknownType", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:59")
  public UnknownType unknownType() {
    ASTState state = state();
    if (unknownType_computed) {
      return unknownType_value;
    }
    if (unknownType_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.unknownType().");
    }
    unknownType_visited = true;
    state().enterLazyAttribute();
    unknownType_value = getParent().Define_unknownType(this, null);
    unknownType_computed = true;
    state().leaveLazyAttribute();
    unknownType_visited = false;
    return unknownType_value;
  }
/** @apilevel internal */
protected boolean unknownType_visited = false;
  /** @apilevel internal */
  private void unknownType_reset() {
    unknownType_computed = false;
    
    unknownType_value = null;
    unknownType_visited = false;
  }
  /** @apilevel internal */
  protected boolean unknownType_computed = false;

  /** @apilevel internal */
  protected UnknownType unknownType_value;

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:206
   * @apilevel internal
   */
  public ASTNode Define_prevNode(ASTNode _callerNode, ASTNode _childNode) {
    int i = this.getIndexOfChild(_callerNode);
    return prevNode(i);
  }
  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:206
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute prevNode
   */
  protected boolean canDefine_prevNode(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  public String Define_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_stmtId(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_stmtId(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/genUniqueName.jrag:9
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute stmtId
   */
  protected boolean canDefine_stmtId(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public Program Define_program(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_program(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_program(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:29
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute program
   */
  protected boolean canDefine_program(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public UnknownDecl Define_unknownDecl(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_unknownDecl(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_unknownDecl(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/UnknownDecl.jrag:4
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute unknownDecl
   */
  protected boolean canDefine_unknownDecl(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_insideCl(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_insideCl(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/rndFix.jrag:14
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideCl
   */
  protected boolean canDefine_insideCl(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_isParameter(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_isParameter(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:18
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isParameter
   */
  protected boolean canDefine_isParameter(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_parameterIndex(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_parameterIndex(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_parameterIndex(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:26
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute parameterIndex
   */
  protected boolean canDefine_parameterIndex(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_localIndex(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_localIndex(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:36
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndex
   */
  protected boolean canDefine_localIndex(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_localIndexc(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_localIndexc(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:57
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndexc
   */
  protected boolean canDefine_localIndexc(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_declInFunc(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_declInFunc(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_declInFunc(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:61
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declInFunc
   */
  protected boolean canDefine_declInFunc(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_insideFunc(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_insideFunc(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:68
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideFunc
   */
  protected boolean canDefine_insideFunc(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_insideClass(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_insideClass(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:78
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute insideClass
   */
  protected boolean canDefine_insideClass(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_decle(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_decle(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_decle(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:85
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute decle
   */
  protected boolean canDefine_decle(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public HashMap<var_def, Integer> Define_objvars(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_objvars(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_objvars(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:163
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objvars
   */
  protected boolean canDefine_objvars(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public HashMap<String, Integer> Define_objNum(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_objNum(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_objNum(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:149
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objNum
   */
  protected boolean canDefine_objNum(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_hasInit(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_hasInit(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_hasInit(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:131
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute hasInit
   */
  protected boolean canDefine_hasInit(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_nbrInit(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_nbrInit(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_nbrInit(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:134
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrInit
   */
  protected boolean canDefine_nbrInit(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public List<var_def> Define_locvars(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_locvars(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_locvars(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:183
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute locvars
   */
  protected boolean canDefine_locvars(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public List<var_def> Define_locvarsF(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_locvarsF(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_locvarsF(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:192
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute locvarsF
   */
  protected boolean canDefine_locvarsF(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public Type Define_expRet(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_expRet(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_expRet(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:9
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute expRet
   */
  protected boolean canDefine_expRet(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_isDeclOut(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_isDeclOut(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_isDeclOut(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:146
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isDeclOut
   */
  protected boolean canDefine_isDeclOut(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_assStmt(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_assStmt(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_assStmt(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:156
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute assStmt
   */
  protected boolean canDefine_assStmt(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public String Define_usedId(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_usedId(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_usedId(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:160
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute usedId
   */
  protected boolean canDefine_usedId(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_isFunction(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_isFunction(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/funcOrVarCh.jrag:8
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isFunction
   */
  protected boolean canDefine_isFunction(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_declIsFunction(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_declIsFunction(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/funcOrVarCh.jrag:13
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute declIsFunction
   */
  protected boolean canDefine_declIsFunction(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_nbrParams(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_nbrParams(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_nbrParams(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/funcOrVarCh.jrag:27
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrParams
   */
  protected boolean canDefine_nbrParams(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public List<Type> Define_params(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_params(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_params(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/funcOrVarCh.jrag:42
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute params
   */
  protected boolean canDefine_params(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public class_def Define_lookupCl(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_lookupCl(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_lookupCl(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:20
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupCl
   */
  protected boolean canDefine_lookupCl(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_getDecl(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_getDecl(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_getDecl(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:109
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute getDecl
   */
  protected boolean canDefine_getDecl(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_isClass(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_isClass(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_isClass(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:113
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute isClass
   */
  protected boolean canDefine_isClass(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_lookupFunc(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_lookupFunc(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_lookupFunc(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:118
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupFunc
   */
  protected boolean canDefine_lookupFunc(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public String Define_type(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_type(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_type(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:184
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute type
   */
  protected boolean canDefine_type(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public Type Define_current(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_current(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_current(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:190
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute current
   */
  protected boolean canDefine_current(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_lookupNN(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_lookupNN(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_lookupNN(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:230
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupNN
   */
  protected boolean canDefine_lookupNN(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_lookupIt(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_lookupIt(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_lookupIt(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:274
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookupIt
   */
  protected boolean canDefine_lookupIt(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_localLookups(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_localLookups(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:290
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localLookups
   */
  protected boolean canDefine_localLookups(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_globalLookup(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_globalLookup(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:344
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute globalLookup
   */
  protected boolean canDefine_globalLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public BoolType Define_boolType(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_boolType(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_boolType(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:4
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute boolType
   */
  protected boolean canDefine_boolType(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public IntType Define_intType(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_intType(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_intType(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:13
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute intType
   */
  protected boolean canDefine_intType(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public AnyType Define_anyType(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_anyType(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_anyType(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:21
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute anyType
   */
  protected boolean canDefine_anyType(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public ListType Define_listType(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_listType(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_listType(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:31
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute listType
   */
  protected boolean canDefine_listType(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public StringType Define_stringType(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_stringType(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_stringType(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:40
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute stringType
   */
  protected boolean canDefine_stringType(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public ObjType Define_objType(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_objType(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_objType(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:50
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute objType
   */
  protected boolean canDefine_objType(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public UnknownType Define_unknownType(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_unknownType(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_unknownType(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:61
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute unknownType
   */
  protected boolean canDefine_unknownType(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public Type Define_types(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_types(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_types(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:48
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute types
   */
  protected boolean canDefine_types(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_nbrLoc(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_nbrLoc(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_nbrLoc(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:97
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrLoc
   */
  protected boolean canDefine_nbrLoc(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public int Define_nbrLocF(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_nbrLocF(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_nbrLocF(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:113
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute nbrLocF
   */
  protected boolean canDefine_nbrLocF(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public var_def Define_getVarDef(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_getVarDef(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_getVarDef(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:6
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute getVarDef
   */
  protected boolean canDefine_getVarDef(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_lookup(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_lookup(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:246
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute lookup
   */
  protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_possAssign(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_possAssign(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_possAssign(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:356
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute possAssign
   */
  protected boolean canDefine_possAssign(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public func_def Define_enclosingFunction(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_enclosingFunction(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_enclosingFunction(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/CallGraph.jrag:8
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute enclosingFunction
   */
  protected boolean canDefine_enclosingFunction(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public IdDecl Define_loclookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_loclookup(self, _callerNode, name)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_loclookup(self, _callerNode, name);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:169
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute loclookup
   */
  protected boolean canDefine_loclookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return false;
  }
  /** @apilevel internal */
  public int Define_localIndexe(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_localIndexe(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_localIndexe(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/paramAdrInd.jrag:83
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute localIndexe
   */
  protected boolean canDefine_localIndexe(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
  /** @apilevel internal */
  public boolean Define_canDeclare(ASTNode _callerNode, ASTNode _childNode) {
    ASTNode self = this;
    ASTNode parent = getParent();
    while (parent != null && !parent.canDefine_canDeclare(self, _callerNode)) {
      _callerNode = self;
      self = parent;
      parent = self.getParent();
    }
    return parent.Define_canDeclare(self, _callerNode);
  }

  /**
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:377
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute canDeclare
   */
  protected boolean canDefine_canDeclare(ASTNode _callerNode, ASTNode _childNode) {
    return false;
  }
public ASTNode rewrittenNode() { throw new Error("rewrittenNode is undefined for ASTNode"); }

}
