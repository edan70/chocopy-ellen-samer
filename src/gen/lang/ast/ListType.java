/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/lang.ast:16
 * @astdecl ListType : Type ::= Type;
 * @production ListType : {@link Type} ::= <span class="component">{@link Type}</span>;

 */
public class ListType extends Type implements Cloneable {
  /**
   * @aspect ListType
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Typen.jrag:32
   */
  public String toString() {
		return "List["+ getType()+"]";
	}
  /**
   * @declaredat ASTNode:1
   */
  public ListType() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Type"},
    type = {"Type"},
    kind = {"Child"}
  )
  public ListType(Type p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:22
   */
  protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    isListType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  public ListType clone() throws CloneNotSupportedException {
    ListType node = (ListType) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public ListType copy() {
    try {
      ListType node = (ListType) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:60
   */
  @Deprecated
  public ListType fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  public ListType treeCopyNoTransform() {
    ListType tree = (ListType) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:90
   */
  public ListType treeCopy() {
    ListType tree = (ListType) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the Type child.
   * @param node The new node to replace the Type child.
   * @apilevel high-level
   */
  public ListType setType(Type node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Type child.
   * @return The current node used as the Type child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Type")
  public Type getType() {
    return (Type) getChild(0);
  }
  /**
   * Retrieves the Type child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Type child.
   * @apilevel low-level
   */
  public Type getTypeNoTransform() {
    return (Type) getChildNoTransform(0);
  }
/** @apilevel internal */
protected boolean isListType_visited = false;
  /** @apilevel internal */
  private void isListType_reset() {
    isListType_computed = false;
    isListType_visited = false;
  }
  /** @apilevel internal */
  protected boolean isListType_computed = false;

  /** @apilevel internal */
  protected boolean isListType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:189
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:188")
  public boolean isListType() {
    ASTState state = state();
    if (isListType_computed) {
      return isListType_value;
    }
    if (isListType_visited) {
      throw new RuntimeException("Circular definition of attribute Type.isListType().");
    }
    isListType_visited = true;
    state().enterLazyAttribute();
    isListType_value = true;
    isListType_computed = true;
    state().leaveLazyAttribute();
    isListType_visited = false;
    return isListType_value;
  }

}
