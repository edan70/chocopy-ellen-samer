/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/lang.ast:62
 * @astdecl Mul : binary ::= Left:cexpr Right:cexpr;
 * @production Mul : {@link binary};

 */
public class Mul extends binary implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/CodeGen.jrag:677
   */
  public void genEval(PrintStream out, IdDecl id) {

		if (getLeft().types().isListType() || getRight().types().isListType()) {
			if (getLeft().types().isListType()) { 
				getLeft().genEval(out,id);
				out.println("        pushq %rax");
				getRight().genEval(out,id);
				out.println("        movq %rax, %rbx");
				out.println("        popq %rax");
			} else {
				getRight().genEval(out,id);
				out.println("        pushq %rax");
				getLeft().genEval(out,id);
				out.println("        movq %rax, %rbx");
				out.println("        popq %rax");
			}
			out.println("# calculating size of the new list");
			out.println("        movq %rbx, %rsi");
			out.println("        movq (%rax), %r9					# moving list size to r9");
			out.println("        imulq %r9, %rsi					# multiplying the number with list size");
			out.println("        movq heap_pointer(%rip), %rdi		# moving the heap pointer to rip");
			out.println("        movq %rsi, (%rdi)					# moving list size to the first available spot in the heap");
			out.println("        pushq %rdi							# pushing address of new list");
			out.println("        addq $8, %rdi						# increasing heap pointer with 8 bytes");

		//	out.println("        imulq 8, %rsi						# multiplying with 8 to get bits");

			out.println("# start looping from 0 to the given number to multiply the list with");
			out.println("        movq $0, %rcx");
			out.println("loop" + stmtId() + ":");
			out.println("        cmp %rbx, %rcx 					# comparing iterator index with the mul number");
			out.println("        jge end_loop" + stmtId());
			out.println("        movq $0, %rsi");

			out.println("inner_loop" + stmtId() + ":			# looping through the list elems and copying them to the new array");
			out.println("        cmp (%rax), %rsi				# comparing iterator index with the list size");
			out.println("        jge end_inner_loop" + stmtId());

			out.println("# copying elements of the list");
			out.println("        movq %rsi, %r8						# moving index of the current element to r8");
			out.println("        addq $1, %r8						# adding 1 to include the first byte with the size");
			out.println("        imulq $8, %r8						# multiplying index with 8 to get bits");
			out.println("        addq %rax, %r8						# adding list address with index bit size and put result in r8");
			out.println("        movq (%r8), %r9					# puts list indexing in r9");
			out.println("        movq %r9, (%rdi)					# adds the element at the adress of r8 to the first available spot in the heap, in the address in rdi");
			out.println("        addq $8, %rdi						# increasing heap pointer with 8 bytes");

			out.println("        addq $1, %rsi						# increasing inner loop counter with 1");
			out.println("        jmp inner_loop" + stmtId());

			out.println("end_inner_loop" + stmtId() + ":");
			out.println("        addq $1, %rcx						# increasing outer loop counter with 1");
			out.println("        jmp loop" + stmtId());
			out.println("end_loop" + stmtId() + ":");
			out.println("        movq %rdi, heap_pointer(%rip)		# moving the heap pointer to rip");
			out.println("        popq %rax							# popping address to the new list to rax");
		} else {
			getLeft().genEval(out,id);
			out.println("        pushq %rax");
			getRight().genEval(out,id);
			out.println("        movq %rax, %rbx");
			out.println("        popq %rax");
			out.println("        imulq %rbx, %rax");
		}
	}
  /**
   * @declaredat ASTNode:1
   */
  public Mul() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Left", "Right"},
    type = {"cexpr", "cexpr"},
    kind = {"Child", "Child"}
  )
  public Mul(cexpr p0, cexpr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
    expectedType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public Mul clone() throws CloneNotSupportedException {
    Mul node = (Mul) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Mul copy() {
    try {
      Mul node = (Mul) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public Mul fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public Mul treeCopyNoTransform() {
    Mul tree = (Mul) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public Mul treeCopy() {
    Mul tree = (Mul) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the Left child.
   * @param node The new node to replace the Left child.
   * @apilevel high-level
   */
  public Mul setLeft(cexpr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Left child.
   * @return The current node used as the Left child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Left")
  public cexpr getLeft() {
    return (cexpr) getChild(0);
  }
  /**
   * Retrieves the Left child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Left child.
   * @apilevel low-level
   */
  public cexpr getLeftNoTransform() {
    return (cexpr) getChildNoTransform(0);
  }
  /**
   * Replaces the Right child.
   * @param node The new node to replace the Right child.
   * @apilevel high-level
   */
  public Mul setRight(cexpr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Right child.
   * @return The current node used as the Right child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Right")
  public cexpr getRight() {
    return (cexpr) getChild(1);
  }
  /**
   * Retrieves the Right child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Right child.
   * @apilevel low-level
   */
  public cexpr getRightNoTransform() {
    return (cexpr) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Type types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:62
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:62")
  public Type types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute Mul.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = types_compute();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
  /** @apilevel internal */
  private Type types_compute() {
  		if (getLeft().types().isListType() ){
  			return getLeft().types();
  		}else if (getRight().types().isListType() ){
  
  			return getRight().types();
  
  		} 
  		return intType();
  		
  	
  	}
/** @apilevel internal */
protected boolean expectedType_visited = false;
  /** @apilevel internal */
  private void expectedType_reset() {
    expectedType_computed = false;
    
    expectedType_value = null;
    expectedType_visited = false;
  }
  /** @apilevel internal */
  protected boolean expectedType_computed = false;

  /** @apilevel internal */
  protected Type expectedType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:137
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:137")
  public Type expectedType() {
    ASTState state = state();
    if (expectedType_computed) {
      return expectedType_value;
    }
    if (expectedType_visited) {
      throw new RuntimeException("Circular definition of attribute Mul.expectedType().");
    }
    expectedType_visited = true;
    state().enterLazyAttribute();
    expectedType_value = types();
    expectedType_computed = true;
    state().leaveLazyAttribute();
    expectedType_visited = false;
    return expectedType_value;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:108
    if (!(typesL().compatibleType(expectedType()) ||  typesL().compatibleType(intType()))) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:113
    if (!(typesR().compatibleType(expectedType()) || typesR().compatibleType(intType()))) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:117
    if ((typesL().isListType() && typesR().isListType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!(typesL().compatibleType(expectedType()) ||  typesL().compatibleType(intType()))) {
      collection.add(error("Expected: '" +expectedType() + "' but recieved: '"+ typesL()+"'"));
    }
    if (!(typesR().compatibleType(expectedType()) || typesR().compatibleType(intType()))) {
      collection.add(error("Expected: '" +expectedType() + "' but recieved: '"+ typesR()+"'"));
    }
    if ((typesL().isListType() && typesR().isListType())) {
      collection.add(error("Cannot multiply two lists"));
    }
  }

}
