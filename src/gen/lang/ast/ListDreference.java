/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/lang.ast:42
 * @astdecl ListDreference : cexpr ::= idCexpr Expr;
 * @production ListDreference : {@link cexpr} ::= <span class="component">{@link idCexpr}</span> <span class="component">{@link Expr}</span>;

 */
public class ListDreference extends cexpr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/CodeGen.jrag:224
   */
  public void genEval(PrintStream out, IdDecl id){
		getidCexpr().genEval(out,id);
		out.println("        pushq  %rax # push address of list ");
		getExpr().genEval(out,id);
		out.println("        pop %rdi   # rdi has the address to the list");
		out.println("        addq $1, %rax  # shifting the address to skip the size");
		out.println("        imulq $8, %rax");
		out.println("        addq %rax, %rdi");
		out.println("        movq (%rdi), %rax");
	}
  /**
   * @declaredat ASTNode:1
   */
  public ListDreference() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"idCexpr", "Expr"},
    type = {"idCexpr", "Expr"},
    kind = {"Child", "Child"}
  )
  public ListDreference(idCexpr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    types_reset();
    correctType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public ListDreference clone() throws CloneNotSupportedException {
    ListDreference node = (ListDreference) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public ListDreference copy() {
    try {
      ListDreference node = (ListDreference) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public ListDreference fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public ListDreference treeCopyNoTransform() {
    ListDreference tree = (ListDreference) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public ListDreference treeCopy() {
    ListDreference tree = (ListDreference) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the idCexpr child.
   * @param node The new node to replace the idCexpr child.
   * @apilevel high-level
   */
  public ListDreference setidCexpr(idCexpr node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the idCexpr child.
   * @return The current node used as the idCexpr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="idCexpr")
  public idCexpr getidCexpr() {
    return (idCexpr) getChild(0);
  }
  /**
   * Retrieves the idCexpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the idCexpr child.
   * @apilevel low-level
   */
  public idCexpr getidCexprNoTransform() {
    return (idCexpr) getChildNoTransform(0);
  }
  /**
   * Replaces the Expr child.
   * @param node The new node to replace the Expr child.
   * @apilevel high-level
   */
  public ListDreference setExpr(Expr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Expr child.
   * @return The current node used as the Expr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Expr")
  public Expr getExpr() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the Expr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Expr child.
   * @apilevel low-level
   */
  public Expr getExprNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean types_visited = false;
  /** @apilevel internal */
  private void types_reset() {
    types_computed = false;
    
    types_value = null;
    types_visited = false;
  }
  /** @apilevel internal */
  protected boolean types_computed = false;

  /** @apilevel internal */
  protected Type types_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:104
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:104")
  public Type types() {
    ASTState state = state();
    if (types_computed) {
      return types_value;
    }
    if (types_visited) {
      throw new RuntimeException("Circular definition of attribute ListDreference.types().");
    }
    types_visited = true;
    state().enterLazyAttribute();
    types_value = ((ListType)getidCexpr().getIdUse().decl().getVarDef().expectedType()).getType();
    types_computed = true;
    state().leaveLazyAttribute();
    types_visited = false;
    return types_value;
  }
/** @apilevel internal */
protected boolean correctType_visited = false;
  /** @apilevel internal */
  private void correctType_reset() {
    correctType_computed = false;
    correctType_visited = false;
  }
  /** @apilevel internal */
  protected boolean correctType_computed = false;

  /** @apilevel internal */
  protected boolean correctType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:126
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:126")
  public boolean correctType() {
    ASTState state = state();
    if (correctType_computed) {
      return correctType_value;
    }
    if (correctType_visited) {
      throw new RuntimeException("Circular definition of attribute ListDreference.correctType().");
    }
    correctType_visited = true;
    state().enterLazyAttribute();
    correctType_value = correctType_compute();
    correctType_computed = true;
    state().leaveLazyAttribute();
    correctType_visited = false;
    return correctType_value;
  }
  /** @apilevel internal */
  private boolean correctType_compute() {
  		return getExpr().types().compatibleType(intType());	
  	}
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:88
    if (!correctType() && getidCexpr().types().isListType()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!correctType() && getidCexpr().types().isListType()) {
      collection.add(error("The dereference expression has type : '" + getExpr().types() + "' and it must be  int"));
    }
  }

}
