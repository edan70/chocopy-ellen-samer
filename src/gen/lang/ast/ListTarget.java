/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.4 */
package lang.ast;
import java.util.*;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
/**
 * @ast node
 * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/lang.ast:80
 * @astdecl ListTarget : Target ::= IdUse Expr;
 * @production ListTarget : {@link Target} ::= <span class="component">{@link Expr}</span>;

 */
public class ListTarget extends Target implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public ListTarget() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  @ASTNodeAnnotation.Constructor(
    name = {"IdUse", "Expr"},
    type = {"IdUse", "Expr"},
    kind = {"Child", "Child"}
  )
  public ListTarget(IdUse p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:23
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushAttrCache() {
    super.flushAttrCache();
    correctType_reset();
    isListTarget_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();

  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public ListTarget clone() throws CloneNotSupportedException {
    ListTarget node = (ListTarget) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public ListTarget copy() {
    try {
      ListTarget node = (ListTarget) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public ListTarget fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  public ListTarget treeCopyNoTransform() {
    ListTarget tree = (ListTarget) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  public ListTarget treeCopy() {
    ListTarget tree = (ListTarget) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Replaces the IdUse child.
   * @param node The new node to replace the IdUse child.
   * @apilevel high-level
   */
  public ListTarget setIdUse(IdUse node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the IdUse child.
   * @return The current node used as the IdUse child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IdUse")
  public IdUse getIdUse() {
    return (IdUse) getChild(0);
  }
  /**
   * Retrieves the IdUse child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IdUse child.
   * @apilevel low-level
   */
  public IdUse getIdUseNoTransform() {
    return (IdUse) getChildNoTransform(0);
  }
  /**
   * Replaces the Expr child.
   * @param node The new node to replace the Expr child.
   * @apilevel high-level
   */
  public ListTarget setExpr(Expr node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Expr child.
   * @return The current node used as the Expr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Expr")
  public Expr getExpr() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the Expr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Expr child.
   * @apilevel low-level
   */
  public Expr getExprNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
/** @apilevel internal */
protected boolean correctType_visited = false;
  /** @apilevel internal */
  private void correctType_reset() {
    correctType_computed = false;
    correctType_visited = false;
  }
  /** @apilevel internal */
  protected boolean correctType_computed = false;

  /** @apilevel internal */
  protected boolean correctType_value;

  /**
   * @attribute syn
   * @aspect typeChecker
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:129
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="typeChecker", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/typeChecker.jrag:129")
  public boolean correctType() {
    ASTState state = state();
    if (correctType_computed) {
      return correctType_value;
    }
    if (correctType_visited) {
      throw new RuntimeException("Circular definition of attribute ListTarget.correctType().");
    }
    correctType_visited = true;
    state().enterLazyAttribute();
    correctType_value = correctType_compute();
    correctType_computed = true;
    state().leaveLazyAttribute();
    correctType_visited = false;
    return correctType_value;
  }
  /** @apilevel internal */
  private boolean correctType_compute() {
  		return getExpr().types().compatibleType(intType());	
  	}
/** @apilevel internal */
protected boolean isListTarget_visited = false;
  /** @apilevel internal */
  private void isListTarget_reset() {
    isListTarget_computed = false;
    isListTarget_visited = false;
  }
  /** @apilevel internal */
  protected boolean isListTarget_computed = false;

  /** @apilevel internal */
  protected boolean isListTarget_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:367
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="/home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/nameanalysis.jrag:366")
  public boolean isListTarget() {
    ASTState state = state();
    if (isListTarget_computed) {
      return isListTarget_value;
    }
    if (isListTarget_visited) {
      throw new RuntimeException("Circular definition of attribute Target.isListTarget().");
    }
    isListTarget_visited = true;
    state().enterLazyAttribute();
    isListTarget_value = true;
    isListTarget_computed = true;
    state().leaveLazyAttribute();
    isListTarget_visited = false;
    return isListTarget_value;
  }
  /** @apilevel internal */
  protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat /home/oem/programming/study/project/chocopy-ellen-samer/src/jastadd/Errors.jrag:92
    if (!correctType()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  /** @apilevel internal */
  protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (!correctType()) {
      collection.add(error("The dereference expression has type : '" + getExpr().types() + "' and it must be  int"));
    }
  }

}
