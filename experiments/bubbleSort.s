.global _start
.data
TRUE: .quad 255
FALSE: .quad 0
heap_pointer: .skip 32
heap_limit: .skip 32
ask_message: .ascii "Please enter a number: "
ask_msg_len: .quad 23
buf: .skip 1024

.text
_start:
        movq $12, %rax        # moving 12 (number of syscall) to rax 
        movq $0, %rdi         # moving 0 to rdi to get current address
        syscall               # getting  current address
        movq %rax, heap_pointer(%rip)      # moving initial heap size to rsi 
        movq $10000, %rsi      # moving initial heap size to rsi 
        movq %rax, %rdx
        addq %rsi, %rdx
        movq %rdx, %rdi       # moving list's size to rdi 
        movq $12, %rax        # moving 12 (number of syscall) to rax 
        syscall               # allocating memory for the list
        movq %rax,  heap_limit(%rip)      # moving initial heap size to rsi 
        pushq %rbp
        movq %rsp, %rbp
        subq $8, %rsp
        movq $1, %rax
        push %rax             # pushing an element of the list to the stack
        movq $43, %rax
        push %rax             # pushing an element of the list to the stack
        movq $122, %rax
        push %rax             # pushing an element of the list to the stack
        movq $13, %rax
        push %rax             # pushing an element of the list to the stack
        movq $66, %rax
        push %rax             # pushing an element of the list to the stack
        movq $4, %rax
        push %rax             # pushing an element of the list to the stack
        movq $111, %rax
        push %rax             # pushing an element of the list to the stack
        movq $0, %rax
        push %rax             # pushing an element of the list to the stack
        movq $3, %rax
        push %rax             # pushing an element of the list to the stack
        movq $19, %rax
        push %rax             # pushing an element of the list to the stack
        movq $20, %rax
        push %rax             # pushing an element of the list to the stack
        movq $11, %rax
        push %rax             # pushing an element of the list to the stack
        movq $10, %rax
        push %rax             # pushing an element of the list to the stack
        movq $5, %rax
        push %rax             # pushing an element of the list to the stack
        movq $7, %rax
        push %rax             # pushing an element of the list to the stack
        movq $15, %rax   # size of the list = 120
        push %rax
        pop %rsi                   	# moving list's size to rsi 
        movq  %rsi, %rcx        			# moving list's size to rcx 
        push %rcx              			# pushing real list size 
        addq $1, %rsi           			# increasing the size to save the size
        imulq  $8, %rsi        			# mul list size with 8  
        pop %rcx              				# moving list's size to rcx 
		 		 movq heap_pointer(%rip) , %rax   # moving the heap pointer to rax
        movq %rcx, (%rax)         		# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,8(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,16(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,24(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,32(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,40(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,48(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,56(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,64(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,72(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,80(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,88(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,96(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,104(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,112(%rax)   	# moving list's element to list 
        pop %rdi              					# moving list elem to rdi 
        movq %rdi,120(%rax)   	# moving list's element to list 
        addq %rsi, heap_pointer(%rip)  	# increasing the size to save the size
        pushq %rax
        movq $100, %rax
        movq %rax, %rbx
        popq %rax
# calculating size of the new list
        movq %rbx, %rsi
        movq (%rax), %r9					# moving list size to r9
        imulq %r9, %rsi					# multiplying the number with list size
        movq heap_pointer(%rip), %rdi		# moving the heap pointer to rip
        movq %rsi, (%rdi)					# moving list size to the first available spot in the heap
        pushq %rdi							# pushing address of new list
        addq $8, %rdi						# increasing heap pointer with 8 bytes
# start looping from 0 to the given number to multiply the list with
        movq $0, %rcx
loop:
        cmp %rbx, %rcx 					# comparing iterator index with the mul number
        jge end_loop
        movq $0, %rsi
inner_loop:			# looping through the list elems and copying them to the new array
        cmp (%rax), %rsi				# comparing iterator index with the list size
        jge end_inner_loop
# copying elements of the list
        movq %rsi, %r8						# moving index of the current element to r8
        addq $1, %r8						# adding 1 to include the first byte with the size
        imulq $8, %r8						# multiplying index with 8 to get bits
        addq %rax, %r8						# adding list address with index bit size and put result in r8
        movq (%r8), %r9					# puts list indexing in r9
        movq %r9, (%rdi)					# adds the element at the adress of r8 to the first available spot in the heap, in the address in rdi
        addq $8, %rdi						# increasing heap pointer with 8 bytes
        addq $1, %rsi						# increasing inner loop counter with 1
        jmp inner_loop
end_inner_loop:
        addq $1, %rcx						# increasing outer loop counter with 1
        jmp loop
end_loop:
        movq %rdi, heap_pointer(%rip)		# moving the heap pointer to rip
        popq %rax							# popping address to the new list to rax
        movq %rax, -8(%rbp)
        subq $8, %rsp
        movq $0, %rax
        movq %rax, -16(%rbp)
        subq $8, %rsp
        movq $1, %rax
        movq %rax, -24(%rbp)
        subq $8, %rsp
        movq $0, %rax
        movq %rax, -32(%rbp)
        subq $8, %rsp
        movq $0, %rax
        movq %rax, -40(%rbp)
        subq $8, %rsp
        movq $0, %rax
        movq %rax, -48(%rbp)
        call stmtSt
        movq %rbp, %rsp
        popq %rbp
        movq $0, %rdi
        movq $60, %rax
        syscall
stmtSt:
        movq -8(%rbp), %rax
        pushq %rax
        call len
        addq $8, %rsp
        movq %rax, -40(%rbp)
        movq $0, %rax
        movq %rax, -16(%rbp)
whilestmt_2_start:
        movq -16(%rbp), %rax
        pushq %rax
        movq -40(%rbp), %rax
        movq %rax, %rbx
        popq %rax
        cmpq %rbx, %rax
        jge whilestmt_2_end
        movq $0, %rax
        movq %rax, -24(%rbp)
whilestmt_2_0_start:
        movq -24(%rbp), %rax
        pushq %rax
        movq -40(%rbp), %rax
        movq %rax, %rbx
        popq %rax
        cmpq %rbx, %rax
        jge whilestmt_2_0_end
        movq -8(%rbp), %rax
        pushq  %rax # push address of list 
        movq -16(%rbp), %rax
        pop %rdi   # rdi has the address to the list
        addq $1, %rax  # shifting the address to skip the size
        imulq $8, %rax
        addq %rax, %rdi
        movq (%rdi), %rax
        pushq %rax
        movq -8(%rbp), %rax
        pushq  %rax # push address of list 
        movq -24(%rbp), %rax
        pop %rdi   # rdi has the address to the list
        addq $1, %rax  # shifting the address to skip the size
        imulq $8, %rax
        addq %rax, %rdi
        movq (%rdi), %rax
        movq %rax, %rbx
        popq %rax
        cmpq %rbx, %rax
        jge ifstmt_2_0_0_end_if
        movq -8(%rbp), %rax
        pushq  %rax # push address of list 
        movq -16(%rbp), %rax
        pop %rdi   # rdi has the address to the list
        addq $1, %rax  # shifting the address to skip the size
        imulq $8, %rax
        addq %rax, %rdi
        movq (%rdi), %rax
        movq %rax, -48(%rbp)
        movq -8(%rbp), %rax
        pushq  %rax # push address of list 
        movq -24(%rbp), %rax
        pop %rdi   # rdi has the address to the list
        addq $1, %rax  # shifting the address to skip the size
        imulq $8, %rax
        addq %rax, %rdi
        movq (%rdi), %rax
        pushq  %rax
        movq -16(%rbp), %rax
        addq $1, %rax
        imulq $8, %rax
        movq -8(%rbp), %rdi
        addq %rax, %rdi
        popq  %rax
        movq %rax, (%rdi)
        movq -48(%rbp), %rax
        pushq  %rax
        movq -24(%rbp), %rax
        addq $1, %rax
        imulq $8, %rax
        movq -8(%rbp), %rdi
        addq %rax, %rdi
        popq  %rax
        movq %rax, (%rdi)
        jmp ifstmt_2_0_0_end_if
ifstmt_2_0_0_end_if:
        movq -24(%rbp), %rax
        pushq %rax
        movq $1, %rax
        movq %rax, %rbx
        popq %rax
        addq %rbx, %rax
        movq %rax, -24(%rbp)
        jmp whilestmt_2_0_start
whilestmt_2_0_end:
        movq -16(%rbp), %rax
        pushq %rax
        movq $1, %rax
        movq %rax, %rbx
        popq %rax
        addq %rbx, %rax
        movq %rax, -16(%rbp)
        jmp whilestmt_2_start
whilestmt_2_end:
        ret
del:
        pushq %rbp
        movq %rsp, %rbp
        movq 16(%rbp), %rax
        movq (%rax), %rax
        popq %rbp
        ret
len:
        pushq %rbp
        movq %rsp, %rbp
        movq 16(%rbp), %rax
        movq (%rax), %rax
        popq %rbp
        ret
# Procedure to print number to stdout.
# C signature: void print(long int)
print:
        pushq %rbp
        movq %rsp, %rbp
        ### Convert integer to string (itoa).
        movq 16(%rbp), %rax
        leaq buf(%rip), %rsi    # RSI = write pointer (starts at end of buffer)
        addq $1023, %rsi
        movb $0x0A, (%rsi)      # insert newline
        movq $1, %rcx           # RCX = string length
        cmpq $0, %rax
        jge itoa_loop
        negq %rax               # negate to make RAX positive
itoa_loop:                      # do.. while (at least one iteration)
        movq $10, %rdi
        movq $0, %rdx
        idivq %rdi              # divide RDX:RAX by 10
        addb $0x30, %dl         # remainder + '0'
        decq %rsi               # move string pointer
        movb %dl, (%rsi)
        incq %rcx               # increment string length
        cmpq $0, %rax
        jg itoa_loop            # produce more digits
itoa_done:
        movq 16(%rbp), %rax
        cmpq $0, %rax
        jge print_end
        decq %rsi
        incq %rcx
        movb $0x2D, (%rsi)
print_end:
        movq $1, %rdi
        movq %rcx, %rdx
        movq $1, %rax
        syscall
        popq %rbp
        ret

# Procedure to read number from stdin.
# C signature: long long int read(void)
input:
        pushq %rbp
        movq %rsp, %rbp
        movq $1, %rax 
        movq $1, %rdi     
        movq $ask_message, %rsi
        movq $25, %rdx
        syscall
        ### R9  = sign
        movq $1, %r9            # sign <- 1
        ### R10 = sum
        movq $0, %r10           # sum <- 0
skip_ws: # skip any leading whitespace
        movq $0, %rdi
        leaq buf(%rip), %rsi
        movq $1, %rdx
        movq $0, %rax
        syscall                 # get one char: sys_read(0, buf, 1)
        cmpq $0, %rax
        jle atoi_done           # nchar <= 0
        movb (%rsi), %cl        # c <- current char
        cmp $32, %cl
        je skip_ws              # c == space
        cmp $13, %cl
        je skip_ws              # c == CR
        cmp $10, %cl
        je skip_ws              # c == NL
        cmp $9, %cl
        je skip_ws              # c == tab
        cmp $45, %cl            # check if negative
        jne atoi_loop
        movq $-1, %r9           # sign <- -1
        movq $0, %rdi
        leaq buf(%rip), %rsi
        movq $1, %rdx
        movq $0, %rax
        syscall                 # get one char: sys_read(0, buf, 1)
atoi_loop:
        cmpq $0, %rax           # while (nchar > 0)
        jle atoi_done           # leave loop if nchar <= 0
        movzbq (%rsi), %rcx     # move byte, zero extend to quad-word
        cmpq $0x30, %rcx        # test if < '0'
        jl atoi_done            # character is not numeric
        cmpq $0x39, %rcx        # test if > '9'
        jg atoi_done            # character is not numeric
        imulq $10, %r10         # multiply sum by 10
        subq $0x30, %rcx        # value of character
        addq %rcx, %r10         # add to sum
        movq $0, %rdi
        leaq buf(%rip), %rsi
        movq $1, %rdx
        movq $0, %rax
        syscall                 # get one char: sys_read(0, buf, 1)
        jmp atoi_loop           # loop back
atoi_done:
        imulq %r9, %r10         # sum *= sign
        movq %r10, %rax         # put result value in RAX
        popq %rbp
        ret

print_string:
        pushq %rbp
        movq %rsp, %rbp
        movq $1, %rdi
        movq 16(%rbp), %rsi
        movq 24(%rbp), %rdx
        movq $1, %rax
        syscall
        popq %rbp
        ret
